
# Coding Project

## Provided Code 

The provided code is a spring-boot application that contains an example controller and a defined API for Editions. For 
the purpose of this challenge you can treat an Edition as a published piece of literature. A JSON schema and multiple
JSON examples are available under the resources directory.

The example endpoints can be found in *ExampleController*. 

### Dependencies 

To start the application both Java 8 and Maven are required.

### Starting up the web server

The application can be started by running `MainApplication.java` in your IDE of choice or by executing the below
command.

```text
mvn spring-boot:run
```

## Functional spec

Create a service that accepts data in the format provided by `Edition` and
generates a top 10 list of words contained in the titles of Editions excluding the definite article **'the'** and the
indefinite articles **'a'** and **'an'**. If there is a tie between two words, such as if the 10th item in the list both
have the value of 35, replace it with the most recently seen value. 

Provide an endpoint to display the top 10 list of words, ordered and with their respective counts. 

Provide any other endpoints that you think would be useful for a consumer of the API. An example of this could be
something like a 'reset data' endpoint. 

## Readme

Create a readme file that includes the following:

* A short description on your approach and why it was chosen. What other approaches were considered, if applicable.
* Trade offs that may have been made and why.
* What you may do differently if this was a service you were building for work without a limited time constraint.
* Anything else you feel is important or want to explain.

**Please do not spend an exorbitant amount of time on this**. It's more for you to provide any additional details on your
approach that may be hard to express via the code or within comments. 


## How we will evaluate

The goal of this code sample is help us identify what you consider production ready code. As a result, focus more on
quality, clarity, and the design of the code rather than implementing the most optimal solution.

Some things we will consider when evaluating your submission: 

* **Architecture**: how clean is the separation of logic between classes and functions. 
* **Correctness**: does your solution solve the problem presented.
* **Readability / Clarity**:  do you explain your solution well and is the code easy to understand.
* **Security**: are there any glaring security holes?
* **Testing**: are there any automated tests, how are they structured?
  * **Full coverage is not required or expected!** Please provide enough examples so that we can get a feel for how you
      approach testing.

## Extras

Swagger UI is also included to make it easy for you to post JSON data up to the server. To access the Swagger UI go to http://localhost:8080/swagger-ui.html

You can expand the example controller and see the endpoint details.
