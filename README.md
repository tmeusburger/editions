# Starting up the app

Run MainApplication through your IDE of choice or execute:

```
mvn spring-boot:run
```

Visit http://localhost:8080/ to view further instructions.

You can also find the instructions file located under

```
/static/instructions.md
```

Replace the readme file with your own personal readme once you're able to access the instructions.
